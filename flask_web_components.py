from flask import Flask, render_template, send_file

class FWC (object):

	custom = []
	standard = []

	def __init__(self,app):
		self.app = app

	def init_app(self,app):
		if not hasattr(app, 'extensions'):
			app.extensions = {}
		app.extensions['fwc'] = self
		#app.add_template_global(self.component_html, 'fwc')
		
	def get_component(self,path):
		print("the requested path: " + path)
		return send_file("bower_components/" + path)
	
	def add_custom_element(self,element):
		self.custom.append(element)

	def __call__(self,*args,**kwargs):
		print("components!")
		return "component html!"

	@property
	def component_html(self):
		webcomponents_js = '<script src="{0}"></script>\n'.format('https://rawgit.com/webcomponents/webcomponents-lite/master/webcomponents-lite.js')

		return webcomponents_js + '<link rel="import" href="hello-world.html">'
