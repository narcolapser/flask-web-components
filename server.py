from flask import Flask, render_template, send_file
from flask_web_components import FWC
app = Flask(__name__)
app.debug = True

fwc = FWC(app)
@app.route("/")
def hello():
	return render_template('index2.html',fwc=fwc)

@app.route("/components/<path:path>")
def component(path):
	print("requested path: " + path)
	return fwc.get_component(path)

@app.route("/hello-world.html")
def custom():
	return send_file("hello-world.html")

if __name__ == "__main__":
	app.run()
